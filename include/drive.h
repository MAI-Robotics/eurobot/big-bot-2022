#ifndef DRIVE_H_
#define DRIVE_H_
#include <math.h>
#include <Arduino.h>
#include "macros.h"

/* data */
/**** Some constants ****/
const float wheelDiameter = 70;         // mm
const float outerWheelDistance = 223.0; // mm
const float wheelWidth = 20.00;         // mm
const float stepsPerRev = 1036 * 2;     // Steps that are needed to do one revolution

// Calculate the values for later calculating the amount of steps
const float midWheelDistance = outerWheelDistance - wheelWidth;
const float distancePerStep = PI * wheelDiameter / stepsPerRev;               // mm
const float anglePerStep = 360 / ((PI * outerWheelDistance) / distancePerStep); //°

// Tolerance in mm added to the stop distance
const float tolerance = 100;
// Minimum distance in wich we have to stop if an obstacle is detected
const float stopDist = 100 * distancePerStep + tolerance;

#define tPerStep 436 // minimal delay between the steps in µs (For 11V: 873, For 22V: 436)
//float tPerSlowStep = tPerStep * 0.5;

class Drive
{
protected:
public:
    Drive(/* args */);
    ~Drive();
    static void DriveFast(unsigned int steps, bool dir, bool ignore);
    static FORCE_INLINE void DriveFast(float distance, bool ignore)
    {
        Drive::DriveFast(abs(distance / distancePerStep), distance > 0, ignore);
    };
    static FORCE_INLINE void DriveFast(float distance)
    {
        Drive::DriveFast(distance, false);
    };

    /// @brief Method to turn the robot. Accepts steps
    /// @param steps    Amount of steps to do
    /// @param dir      Direction the robot will turn
    /// @param ignore   Don't use Collision Avoidance
    static void TurnFast(unsigned int steps, bool dir, bool ignore);
    static FORCE_INLINE void TurnFast(float angle, bool ignore)
    {
        Drive::TurnFast(abs(angle / anglePerStep), angle > 0, ignore);
    };
    static FORCE_INLINE void TurnFast(float angle)
    {
        Drive::TurnFast(angle, false);
    }
};

#endif //  DRIVE_H_