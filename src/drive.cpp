#include "drive.h"
#include "pins.h"
#include "fastio.h"

Drive::Drive(/* args */)
{
}

Drive::~Drive()
{
}

void Drive::DriveFast(unsigned int steps, bool dir, bool ignore)
{
    WRITE(DIR_LEFT_PIN, dir ? 1 : 0);
    WRITE(DIR_RIGHT_PIN, dir ? 0 : 1);
    WRITE(ENABLE_PIN, LOW);

    delay(10);

    float a = 0; // for acceleration
    for (unsigned int i = 0; i < steps; i++)
    {
        a = 0;
        if (i < 400 && i <= steps / 2)
        {
            a = tPerStep * 0.0025 * (400 - i);
        }

        if (steps - i < 400 && i > steps / 2)
        {
            a = tPerStep * 0.0025 * (400 - (steps - i));
        }

        if (dir? READ(IR) : READ(IR_BACK) && steps - i > 200 && !ignore)
        {
            for (int blahh = 0; blahh < 100; blahh++)
            {
                a = tPerStep * 0.0025 * (100 - blahh);

                WRITE(STEP_PIN, HIGH);

                delayMicroseconds(tPerStep + a);
                WRITE(STEP_PIN, LOW);

                delayMicroseconds(tPerStep + a);
            }

            WRITE(ENABLE_PIN, HIGH);
            while (READ(IR))
            {
                WRITE(13, HIGH);
                delay(250);
                WRITE(13, LOW);
                delay(250);
            }
            WRITE(ENABLE_PIN, LOW);

            for (int blubb = 0; blubb < 100; blubb++)
            {
                a = tPerStep * 0.0025 * (100 - blubb);

                WRITE(STEP_PIN, HIGH);

                delayMicroseconds(tPerStep + a);
                WRITE(STEP_PIN, LOW);

                delayMicroseconds(tPerStep + a);
            }

            i += 200;
        }

        WRITE(STEP_PIN, HIGH);

        delayMicroseconds(tPerStep + a);
        WRITE(STEP_PIN, LOW);

        delayMicroseconds(tPerStep + a);
    }

    delay(10);
    WRITE(ENABLE_PIN, HIGH);
}

void Drive::TurnFast(unsigned int steps, bool dir, bool ignore)
{
    WRITE(DIR_LEFT_PIN, dir ? 1 : 0);
    WRITE(DIR_RIGHT_PIN, dir ? 1 : 0);
    WRITE(ENABLE_PIN, LOW);

    delay(10);

    float a = 0; // for acceleration
    for (unsigned int i = 0; i < steps; i++)
    {
        a = 0;
        if (i < 400 && i <= steps / 2)
        {
            a = tPerStep * 0.0025 * (400 - i);
        }

        if (steps - i < 400 && i > steps / 2)
        {
            a = tPerStep * 0.002 * (400 - (steps - i));
        }

        if ((READ(IR_LEFT) || READ(IR_RIGHT)) && steps - i > 200 && !ignore)
        {
            for (int blahh = 0; blahh < 100; blahh++)
            {
                a = tPerStep * 0.0025 * (100 - blahh);

                WRITE(STEP_PIN, HIGH);

                delayMicroseconds(tPerStep + a);
                WRITE(STEP_PIN, LOW);
                delayMicroseconds(tPerStep + a);
            }

            WRITE(ENABLE_PIN, HIGH);
            while (READ(IR_LEFT) || READ(IR_RIGHT))
            {
                WRITE(13, HIGH);
                delay(250);
                WRITE(13, LOW);
                delay(250);
            }

            WRITE(ENABLE_PIN, LOW);

            for (int blubb = 0; blubb < 100; blubb++)
            {
                a = tPerStep * 0.0025 * (100 - blubb);

                WRITE(STEP_PIN, HIGH);

                delayMicroseconds(tPerStep + a);
                WRITE(STEP_PIN, LOW);

                delayMicroseconds(tPerStep + a);
            }

            i += 200;
        }

        WRITE(STEP_PIN, HIGH);

        delayMicroseconds(tPerStep + a);
        WRITE(STEP_PIN, LOW);

        delayMicroseconds(tPerStep + a);
    }

    WRITE(ENABLE_PIN, HIGH);
}