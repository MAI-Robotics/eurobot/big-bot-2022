#include <Arduino.h>
#include "drive.h"
#include "pins.h"
#include <TimerTwo.h>
#include "fastio.h"
#include <Servo.h>

Servo front;
volatile unsigned long startTime;

#define MATCH_TIME 10ul // seconds
const unsigned long MATCH_TIME_MS =  MATCH_TIME * 1000ul;

void heartbeat()
{
  //TOGGLE(LED_BUILTIN);
  if (millis() - startTime > MATCH_TIME_MS)
  {
    WRITE(LED_BUILTIN, 1);
    TCCR2B = 0;
    TCCR1B = 0;
    TCCR0B = 0;
  }
}

void setup()
{
  Serial.begin(115200);
  /// Set Pin Modes
  SET_OUTPUT(LED_BUILTIN);
  SET_OUTPUT(STEP_PIN);
  SET_OUTPUT(ENABLE_PIN);
  SET_OUTPUT(DIR_LEFT_PIN);
  SET_OUTPUT(DIR_RIGHT_PIN);
  SET_INPUT(IR);
  SET_INPUT(IR_LEFT);
  SET_INPUT(IR_RIGHT);
  SET_INPUT_PULLUP(PULLCORD);
  SET_INPUT_PULLUP(TEAM_SELECT);

  //Timer2.init(65535, heartbeat);
  //Timer2.attachInterrupt(heartbeat);

  front.attach(FRONT_SERVO_PIN);
  front.write(90);

  /// Wait for pull to start
  while (!READ(PULLCORD))
    delay(10);
  //Timer2.start();
  //startTime = millis();
  front.write(45);
  delay(1000);
}

void teamYellow() {
  delay(15000);
  Drive::TurnFast(250, true);
  Drive::DriveFast(1100);
  Drive::TurnFast(-250);
  Drive::DriveFast(1100);
  Drive::DriveFast(-400);
  Drive::TurnFast(-225);
  Drive::DriveFast(1200);
}

void teamPurple() {
  delay(15000);
  Drive::TurnFast(-250, true);
  Drive::DriveFast(1100);
  Drive::TurnFast(250);
  Drive::DriveFast(1100);
  Drive::DriveFast(-400);
  Drive::TurnFast(225);
  Drive::DriveFast(1200);
}

void loop()
{
  if (READ(TEAM_SELECT))
  teamYellow();
  else
  teamPurple();
  while (1)
    delay(1000);
}